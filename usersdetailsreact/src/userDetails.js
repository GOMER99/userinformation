import React, { Component } from 'react';
import Modal from 'react-modal';
import axios from 'axios';
import './App.css';
import './bootstrap.min.css';

const customStyles = {
  content : {
    top                   : '50%',
    left                  : '50%',
    right                 : 'auto',
    bottom                : 'auto',
    marginRight           : '-50%',
    transform             : 'translate(-50%, -50%)'
  }
};

class UserDetails extends Component {
    constructor(props) {
      super(props);
      this.state = {
          apiResponse: "",
          usersList: [],
          userDetails: { 
            user_id : "", 
            last_name: "", 
            first_name: "",
            birth_date: "",
            mobile_number: "",
            email_address: "",
            clickedEdit: false,
          },
          modalIsOpen: false,
          url: 'http://localhost:9000/users',
          getUsers: 'http://localhost:9000/getUsers',
          updateUsers: 'http://localhost:9000/updateUsers',
          deleteUser: 'http://localhost:9000/deleteUsers'
      }
      this.openModal = this.openModal.bind(this);
      this.closeModal = this.closeModal.bind(this);
      new Date();
    }

    componentDidMount() {
      this.sendUserDetails(this.state.getUsers, '');
    }

    openModal() {
      this.setState({modalIsOpen: true});
    }
    
    closeModal() {
      this.setState({modalIsOpen: false});
    }

    async sendUserDetails(url, data) {
      console.log("Url", url);
      console.log("Data", data);
        const options = {
        method: 'POST',
        headers: { 'content-type': 'application/json' },
        data,
        url: url,
      };
    
      const response = await axios(options);
      console.log("Response", response);
      this.setState({usersList: response.data.userDetails});      
    }    

    makeid(length) {
      var result           = '';
      var characters       = '0123456789';
      var charactersLength = characters.length;
      for ( var i = 0; i < length; i++ ) {
         result += characters.charAt(Math.floor(Math.random() * charactersLength));
      }
      return parseInt(result);
   }

  render() {
    return(     
      <div className="container" style={{overflow: 'auto'}}>
        <h2>User Details</h2>
            <table className="table">
              <thead>
                <tr>
                  <th>User ID</th>
                  <th>Last Name</th>
                  <th>First Name</th>
                  <th>Date of Birth</th>
                  <th>Mobile Number</th>
                  <th>Email</th>
                  <th>Actions</th>
                </tr>
              </thead>
              
              <tbody>
               {(this.state.usersList.length > 0) ? this.state.usersList.map((list, index) => {
                return (
                <tr>                  
                    {Object.keys(list).map((listKey) => {                  
                  return(
                    <td>{this.state.usersList[index][listKey]}</td>
                  )})}
                  <td className="cursor-pointer" onClick={(event) => { this.setState({userDetails: list, clickedEdit: true}); this.openModal(); }}>Edit</td>
                  <td className="cursor-pointer" onClick={(event) => { this.sendUserDetails( this.state.deleteUser, JSON.stringify( {"user_id": list.user_id}) )}}>Delete</td>             
                </tr>
                )}) : "Submit the User Details" }               
              </tbody>
              
          </table>
          <button class="btn btn-primary" type="submit" onClick={(event) => { Object.keys(this.state.userDetails).map((key) => { this.state.userDetails[key] = ""}); this.setState({userDetails: this.state.userDetails, clickedEdit: false}); this.openModal() }}>Submit User Details</button>
          <Modal
            isOpen={this.state.modalIsOpen}
            onAfterOpen={this.afterOpenModal}
            onRequestClose={ (event) => { this.closeModal(); }}
            style={customStyles}
            contentLabel="Example Modal"
        >

            {/* <h2 ref={_subtitle => (subtitle = _subtitle)}>Hello</h2> */}
            <button onClick={ (event) => { this.closeModal(); }}>close</button>
            <div>Enter User Details</div>
            
                <div class="form-group">                  
                    <label id="first" for="firstname">First Name</label>
                    <input type="text" class="form-control" id="firstname" placeholder="Enter First Name" 
                      value = { this.state.userDetails["first_name"] }
                      onChange={(event) => {this.state.userDetails["first_name"] = event.target.value; this.setState({userDetails: this.state.userDetails}); }}
                    />
                </div>
                <div class="form-group">     
                  <label for="lastname">Last Name</label>
                  <input type="text" class="form-control" id="lastname" placeholder="Enter Last Name"
                    value = { this.state.userDetails["last_name"] }
                    onChange={(event) => {this.state.userDetails["last_name"] = event.target.value; this.setState({userDetails: this.state.userDetails}); }} 
                  />
                </div>
                <div class="form-group">
                  <label for="dateofbirth">Date of Birth</label>
                  <input type="text" class="form-control" id="dateofbirth" placeholder="YYYY-MM-DD"
                    value={ this.state.userDetails["birth_date"] } 
                    onChange={(event) => {this.state.userDetails["birth_date"] = event.target.value; this.setState({userDetails: this.state.userDetails}); }}
                  />
                </div>
                <div class="form-group">
                  <label for="mobilenumber">Mobile Number</label>
                  <input type="number" class="form-control" id="mobilenumber" placeholder="Enter Mobile Number"
                    value={ this.state.userDetails["mobile_number"] }
                    onChange={(event) => {this.state.userDetails["mobile_number"] = event.target.value; this.setState({userDetails: this.state.userDetails}); }} 
                  />
                </div>
                <div class="form-group">
                  <label for="emailaddress">Email address</label>
                  <input type="email" class="form-control" id="emailaddress" aria-describedby="emailHelp" placeholder="Enter email"
                    value={ this.state.userDetails["email_address"] }
                    onChange={(event) => {this.state.userDetails["email_address"] = event.target.value; this.setState({userDetails: this.state.userDetails}); }} 
                  />
                </div>                
                <button type="submit" class="btn btn-primary" onClick={(event) => { 
                                if(!this.state.clickedEdit) { 
                                    this.state.userDetails["user_id"] = this.makeid(3); 
                                    this.sendUserDetails(this.state.url, JSON.stringify(this.state.userDetails));
                                } 
                                else {
                                  this.sendUserDetails(this.state.updateUsers, JSON.stringify(this.state.userDetails));
                                }
                }}>Submit</button>
            
        </Modal>
      </div>      
    )
  }
}

export default UserDetails;