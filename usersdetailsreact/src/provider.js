import React, {Component} from 'react';
import { AppContext } from './context.js';

export class AppProvider extends Component {
    state = {
        CGPA: "",

        setCGPA: (cgpa) => {
            this.setState({CGPA: cgpa})
        },

    }
    render() {
        return <AppContext.Provider value={this.state} >{this.props.children}</AppContext.Provider>
    }

}
