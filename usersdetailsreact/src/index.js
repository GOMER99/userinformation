// import React from 'react';
// import ReactDOM from 'react-dom';
// import './index.css';
// import App from './App';
// import reportWebVitals from './reportWebVitals';

// ReactDOM.render(
//   <React.StrictMode>
//     <App />
//   </React.StrictMode>,
//   document.getElementById('root')
// );

// // If you want to start measuring performance in your app, pass a function
// // to log results (for example: reportWebVitals(console.log))
// // or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
// reportWebVitals();


import React from 'react';
import ReactDOM from 'react-dom';
import { AppProvider } from './provider.js';
import { ToastContainer } from 'react-toastify';
import './index.css';
import UserDetails from './userDetails.js';
// import * as serviceWorker from './serviceWorker.js';

document.addEventListener('DOMContentLoaded', function() {
ReactDOM.render(
    <div>
    <ToastContainer
        position='top-right'
        autoClose={4000}
        hideProgressBar
        newestOnTop
        closeOnClick
        rtl={false}
        pauseOnVisibilityChange
        draggable
        pauseOnHover
    />
    <AppProvider> <UserDetails /></AppProvider> 
</div>,
    document.getElementById('root'));
    // serviceWorker.unregister();
});

